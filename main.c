#include "srcdefs.h"
#include <Windows.h>
#include <stdio.h>
#include <assert.h>
#include <math.h>
#include <SDL.h>
#include <SDL2/SDL_ttf.h>

#include <intrin.h>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#define SCC(c)                                                                         \
if ( ( c ) < 0 )                                                                       \
{                                                                                      \
	fprintf ( stderr, "In %s at %d in %s\n", __FILE__, __LINE__, __FUNCTION__ );       \
	fprintf ( stderr, "%s\n", SDL_GetError ( ) );                                      \
	return ( c );                                                                      \
}                                                      

#define SCP(p)                                                                         \
if ( !( p ) )                                                                          \
{                                                                                      \
	fprintf ( stderr, "Error in %s at %d in %s\n", __FILE__, __LINE__, __FUNCTION__ ); \
	fprintf ( stderr, "%s\n", SDL_GetError ( ) );                                      \
	return  ( 0 );                                                                     \
}

#define WINDOW_WIDTH  1280
#define WINDOW_HEIGHT 720

#define HEX_COLOR(x) ( ( x ) >> ( 3 * 8 ) ) & 0xFF, \
                     ( ( x ) >> ( 2 * 8 ) ) & 0xFF, \
                     ( ( x ) >> ( 1 * 8 ) ) & 0xFF, \
                     ( ( x ) >> ( 0 * 8 ) ) & 0xFF

#define CLAMP(value,min,max) ( ( value ) < ( min ) ? ( min ) : ( ( value ) > ( max ) ) ? ( max ) : ( value ) )

i32 screen_clear ( SDL_Renderer *renderer, u32 color )
{
     SCC ( SDL_SetRenderDrawColor ( renderer,  HEX_COLOR ( color ) ) );
     SCC ( SDL_RenderClear ( renderer ) );
	 
	 return 0;
}

#define CIF_WIDTH  352
#define CIF_HEIGHT 288


u8 RGB  [ CIF_HEIGHT * CIF_WIDTH * 4             ] = { 0 };
u8 Y1   [   CIF_WIDTH * CIF_HEIGHT               ] = { 0 };
u8 U1   [ ( CIF_WIDTH / 2 ) * ( CIF_HEIGHT / 2 ) ] = { 0 };
u8 V1   [ ( CIF_WIDTH / 2 ) * ( CIF_HEIGHT / 2 ) ] = { 0 };

u8 Y2 [ CIF_WIDTH * CIF_HEIGHT ] = { 0 };
u8 U2 [ ( CIF_WIDTH / 2 ) * ( CIF_HEIGHT / 2 ) ] = { 0 };
u8 V2 [ ( CIF_WIDTH / 2 ) * ( CIF_HEIGHT / 2 ) ] = { 0 };


u8 clamp ( i32 v, i32 min, i32 max )
{
	return ( v < min ? min : ( v > max ? max : v ) );
}


void normalized_cross_correlation ( )
{

}


// Cross Correlation
void cross_correlation ( )
{

}


// Phase Correlation
void phase_correlation ( )
{

}


void mean_absolute_deviation ( )
{

}

void median_absolute_deviation ( )
{

}

void average_absolute_deviation ( )
{

}

// Rectangular Window ( a.k.a Boxcar or Dirichlet Window )



// B-Spline Window


// Triangular Window


// Welch Window


// Sine Window

// Power-of-Sine Window


// Power-of-Cosine Window


// Cosine Sum Window


// Hann Window


// Hann-and-Hanning Window


// Blackman-Window


// Blackman-Nuttall Window


// Blackman-Harris Window


// Flat-Top Window


// Rife-Vincent Window


// Gaussian Window


// Confined Guassian Window


// Approximate Confined Gaussian Window


// Generalized Normal Window


// Tukey Window


// Planck-Taper Window


// DPSS or Slepian Window


// Kaiser Window


// Dolph�Chebyshev Window


// Ultraspherical Window


// Exponential or Poisson Window


// Bartlett�Hann Window


// Planck�Bessel Window


// Hann�Poisson Window


// MAD: Mean Absolute Difference
f64 mean_absolute_difference ( u8* current, u8* reference, u32 block_size )
{
	u32 sum = 0;
	for ( u32 y = 0; y < block_size; y++ )
	{
		for ( u32 x = 0; x < block_size; x++ )
		{
			u32 i = x + ( y * block_size );
			sum += abs ( current [ i ] - reference [ i ] );
		}
	}
	return ( sum / ( block_size * block_size ) );
}


f64 arithmetic_mean ( u8* block, u32 block_size )
{
	f64 sum = 0;
	for ( u32 y = 0; y < block_size; y++ )
	{
		for ( u32 x = 0; x < block_size; x++ )
		{
			u32 i = x + ( y * block_size );
			sum += block [ i ];
		}
	}
	return ( ( f64 ) sum / pow ( block_size, 2 ) );
}

f64 AM ( u8* block, u32 block_size )
{
	f64 sum = 0;
	for ( u32 i = 0; i < block_size; i++ )
	{
		sum += ( f64 ) block [ i ];
	}
	return ( sum / ( f64 ) block_size );
}


f64 geometric_mean ( u8* block, u32 block_size )
{
	f64 sum = 1;
	for ( u32 y = 0; y < block_size; y++ )
	{
		for ( u32 x = 0; x < block_size; x++ )
		{
			u32 i = x + ( y * block_size );
			sum *= block [ i ];
		}
	}
	return ( pow ( sum, ( 1.0 / pow ( block_size, 2 ) ) ) );
}

f64 GM ( u8* block, u32 block_size )
{
	f64 sum = 1;
	for ( u32 i = 0; i < block_size; i++ )
	{
		sum *= block [ i ];
	}
	return ( pow ( sum, ( 1.0 / block_size ) ) );
}


f64 harmonic_mean ( u8* block, u32 block_size )
{
	f64 sum = 0;
	for ( u32 y = 0; y < block_size; y++ )
	{
		for ( u32 x = 0; x < block_size; x++ )
		{
			u32 i = x + ( y * block_size );
			sum += ( ( f64 ) 1.0 / ( f64 ) block [ i ] );
		}
	}
	return ( pow ( block_size, 2 ) / sum );
}


f64 HM ( u8* block, u32 block_size )
{
	f64 sum = 0;
	for ( u32 i = 0; i < block_size; i++ )
	{
		sum += ( ( f64 ) 1.0 / ( f64 ) block [ i ] );
	}
	return ( ( f64 ) block_size / sum );
}




// MSE: Mean Squared Error
f64 mean_squared_error ( u8* current, u8* reference, u32 block_size )
{
	f64 sum = 0;
	for ( u32 y = 0; y < block_size; y++ )
	{
		for ( u32 x = 0; x < block_size; x++ )
		{
			u32 i = x + ( y * block_size );
			sum += ( ( current [ i ] - reference [ i ] ) * ( current [ i ] - reference [ i ] ) );
		}
	}
	return ( sum / pow ( block_size, 2 ) );
}

// PSNR: Peak Signal To Noise Ratio
u32 peak_signal_to_noise_ratio ( )
{
	return 0;
}

// TSS
void three_step_search ( )
{

}

// FSS
void four_step_search ( )
{

}


// TDL
void two_dimensional_logarithmic_search ( )
{

}

// BS
void binary_search ( )
{

}

// OSA
void orthogonal_search_algorithm ( )
{

}

// OTA
void one_at_a_time_algorithm ( )
{

}

void luma_dct ( u8* luma_frame, u32 width, u32 height, u32 block_size )
{
	u8 luma8x8   [ 8  ] [ 8  ] = { 0 };
	u8 dct8x8    [ 8  ] [ 8  ] = { 0 };
	u8 luma16x16 [ 16 ] [ 16 ] = { 0 };
	
	for ( u32 h = 0; h < height; h += block_size )
	{
		for ( u32 w = 0; w < width; w += block_size )
		{
			for ( u32 y = 0; y < block_size; y++ )
			{
				for ( u32 x = 0; x < block_size; x++ )
				{
					u32 i = x + ( y * block_size );
					luma8x8 [ y ] [ x ] = luma_frame [ i ];	
				}
			}

			for ( u32 u = 0; u < block_size; ++u )
			{
				for ( u32 v = 0; v < block_size; ++v )
				{
					dct8x8 [ u ][ v ] = 0;
					for ( u32 i = 0; i < block_size; i++ )
					{
						for ( u32 j = 0; j < block_size; j++ )
						{
							dct8x8 [ u ][ v ] += luma8x8 [ i ][ j ] * cos ( M_PI / ( ( f32 ) block_size ) * ( i + 1.0f / 2.0f ) * u )
																	* cos ( M_PI / ( ( f32 ) block_size ) * ( j + 1.0f / 2.0f ) * v );
						}
					}
				}
			}


		}
	}
}


#define HANDLE_CLOSE_SAFE(h) \
if ( ( h ) )                 \
{                            \
	CloseHandle ( ( h ) );   \
	( h ) = 0;               \
}




#ifdef _MSC_VER

void* mmap ( const i8* filepath, u64 *size )
{
	HANDLE hFile = CreateFileA ( filepath,
								 GENERIC_READ,
							     0,
								 0,
								 OPEN_EXISTING,
								 FILE_ATTRIBUTE_NORMAL | 
								 FILE_FLAG_SEQUENTIAL_SCAN,
								 0 );
	GetFileSizeEx ( hFile, ( LARGE_INTEGER* ) size );
	HANDLE hMappedFile = CreateFileMappingA ( hFile,  0, PAGE_READONLY | SEC_COMMIT, 0, 0, 0 );
	void* buffer = ( void* ) MapViewOfFile  ( hMappedFile, FILE_MAP_READ, 0, 0, *size );
	
	HANDLE_CLOSE_SAFE ( hFile );
	HANDLE_CLOSE_SAFE ( hMappedFile );

	return buffer;
}


void munmap ( void* m )
{
	UnmapViewOfFile ( m );
}	



#endif // _MSC_VER


void parse_x264_nal ( )
{
	
}


enum x86_instructions_t
{
	SSE,
    SSE2,
	SSE3,
	SSSE3,
	SSE41,
    SSE42,
	SSE4a,
	
	AVX,
	AVX2,
	AVX512F,
	AVX512PF,
    AVX512ER,
    AVX512CD,
	
    PCLMULQD,
    MONITOR,
    FMA,
    CMPXCHG1,
    MOVBE,
    POPCNT,
    AES,
    XSAVE,
    OSXSAVE,
    F16C,
    RDRAND,
    MSR,
    CX8,
    SEP,
    CMOV,
    CLFSH,
    MMX,
    FXSR,
    FSGSBASE,
    BMI1,
    HLE,
    BMI2,
    ERMS,
    INVPCID,
    RTM,
    RDSEED,
    ADX,
    SHA,
    PREFETCH,
    LAHF,
    LZCNT,
    ABM,
    XOP,
    TBM,
    SYSCALL,
    MMXEXT,
    RDTSCP,
    AMD_3DNOWEX,
    AMD_3DNOW,
};



enum chroma_subsampling_t
{
	YUV444,
	YUV440,
	YUV422,
	YUV420,
	YUV411,
	YUV410
};



void rgba2yuv420 ( u8* rgba, i32 width, i32 height, u8** Y, u8** U, u8** V )
{

	i32 idx;
	i32 sum [ 2 ];

	i32 r [ 2 ], g [ 2 ], b [ 2 ];
	u8* pos  = 0;
	u8* line = 0;
	
	*Y = ( u8* ) malloc ( width * height );
	*U = ( u8* ) malloc ( ( width / 2 ) * ( height / 2 ) );
	*V = ( u8* ) malloc ( ( width / 2 ) * ( height / 2 ) );
	
	idx  = 0; 
	line = rgba;
	
	for ( i32 j = 0 ; j < height ; j++ )
	{
		pos = line;
		for ( i32 i = 0 ; i < width ; i++ )
		{
			r [ 0 ] = *pos; pos++;
			g [ 0 ] = *pos; pos++;
			b [ 0 ] = *pos; pos += 2;
			( *Y ) [ idx ] = ( (  66 * r [ 0 ] + 129 * g [ 0 ] +  25 * b [ 0 ] + 128 ) >> 8 ) + 16;
			idx++;
		}
		line += width * 4;
	}


	idx = 0; line = rgba;
	for ( i32 j = 0; j < height; j += 2 )
	{
		pos = line;
		for ( i32 i = 0; i < width; i += 2 )
		{

			r [ 0 ] = *pos; 
			r [ 1 ] = *( pos + width * 4 ); pos++;
			
			g [ 0 ] = *pos; 
			g [ 1 ] = *( pos + width * 4 ); pos++;

			b [ 0 ] = *pos; 
			b [ 1 ] = *( pos + width * 4 ); pos += 2;

			sum [ 0 ] = ( ( -38 * r [ 0 ] - 74 * g [ 0 ] + 112 * b [ 0 ] + 128 ) >> 8 ) + 128;
			sum [ 1 ] = ( ( 112 * r [ 0 ] - 94 * g [ 0 ] - 18  * b [ 0 ] + 128 ) >> 8 ) + 128;

			sum [ 0 ] += ( ( -38 * r [ 1 ] - 74 * g [ 1 ] + 112 * b [ 1 ] + 128 ) >> 8 ) + 128;
			sum [ 1 ] += ( ( 112 * r [ 1 ] - 94 * g [ 1 ] - 18  * b [ 1 ] + 128 ) >> 8 ) + 128;

			r [ 0 ] = *pos; 
			r [ 1 ] = *( pos + width * 4 ); 
			pos++;

			g [ 0 ] = *pos; 
			g [ 1 ] = *( pos + width * 4 ); 
			pos++;

			b [ 0 ] = *pos; 
			b [ 1 ] = *( pos + width * 4 ); 
			pos += 2;

			sum [ 0 ] += ( ( -38 * r [ 0 ] - 74 * g [ 0 ] + 112 * b [ 0 ] + 128 ) >> 8 ) + 128;
			sum [ 1 ] += ( ( 112 * r [ 0 ] - 94 * g [ 0 ] - 18  * b [ 0 ] + 128 ) >> 8 ) + 128;

			sum [ 0 ] += ( ( -38 * r [ 1 ] - 74 * g [ 1 ] + 112 * b [ 1 ] + 128 ) >> 8 ) + 128;
			sum [ 1 ] += ( ( 112 * r [ 1 ] - 94 * g [ 1 ] - 18  * b [ 1 ] + 128 ) >> 8 ) + 128;

			( *U ) [ idx ] = sum [ 0 ] / 4;
			( *V ) [ idx ] = sum [ 1 ] / 4;

			idx++;
		}
		line += 2 * width * 4;
	}
}


void rgb2yuv420 ( u8* rgb, i32 width, i32 height, u8** Y, u8** U, u8** V )
{
	i32 idx       =   0;
	i32 sum [ 2 ] = { 0 };
	i32   r [ 2 ] = { 0 };
    i32	  g [ 2 ] = { 0 };
    i32	  b [ 2 ] = { 0 };
	u8*   pos     =   0;
	u8*   line    =   0;
	
	*Y = ( u8* ) malloc ( width * height );
	*U = ( u8* ) malloc ( ( width / 2 ) * ( height / 2 ) );
	*V = ( u8* ) malloc ( ( width / 2 ) * ( height / 2 ) );
	
	idx  = 0; 
	line = rgb;
	
	for ( i32 j = 0 ; j < height ; j++ )
	{
		pos = line;
		for ( i32 i = 0 ; i < width ; i++ )
		{
			r [ 0 ] = *pos; 
			pos++;
			
			g [ 0 ] = *pos;
			pos++;
			
			b [ 0 ] = *pos;
			pos++;
			
			( *Y ) [ idx ] = ( (  66 * r [ 0 ] + 129 * g [ 0 ] +  25 * b [ 0 ] + 128 ) >> 8 ) + 16;
			idx++;
		}
		line += width * 3;
	}

	idx = 0; line = rgb;
	for ( i32 j = 0; j < height; j += 2 )
	{
		pos = line;
		for ( i32 i = 0; i < width; i += 2 )
		{
			r [ 0 ] = *pos; 
			r [ 1 ] = *( pos + width * 3 ); 
			pos++;
			
			g [ 0 ] = *pos; 
			g [ 1 ] = *( pos + width * 3 ); 
			pos++;

			b [ 0 ] = *pos; 
			b [ 1 ] = *( pos + width * 3 ); 
			pos++;

			sum [ 0 ] = ( ( -38  * r [ 0 ] - 74 * g [ 0 ] + 112 * b [ 0 ] + 128 ) >> 8 ) + 128;
			sum [ 1 ] = ( ( 112  * r [ 0 ] - 94 * g [ 0 ] - 18  * b [ 0 ] + 128 ) >> 8 ) + 128;

			sum [ 0 ] += ( ( -38 * r [ 1 ] - 74 * g [ 1 ] + 112 * b [ 1 ] + 128 ) >> 8 ) + 128;
			sum [ 1 ] += ( ( 112 * r [ 1 ] - 94 * g [ 1 ] - 18  * b [ 1 ] + 128 ) >> 8 ) + 128;

			r [ 0 ] = *pos; 
			r [ 1 ] = *( pos + width * 3 ); 
			pos++;

			g [ 0 ] = *pos; 
			g [ 1 ] = *( pos + width * 3 ); 
			pos++;

			b [ 0 ] = *pos; 
			b [ 1 ] = *( pos + width * 3 ); 
			pos++;

			sum [ 0 ] += ( ( -38 * r [ 0 ] - 74 * g [ 0 ] + 112 * b [ 0 ] + 128 ) >> 8 ) + 128;
			sum [ 1 ] += ( ( 112 * r [ 0 ] - 94 * g [ 0 ] - 18  * b [ 0 ] + 128 ) >> 8 ) + 128;

			sum [ 0 ] += ( ( -38 * r [ 1 ] - 74 * g [ 1 ] + 112 * b [ 1 ] + 128 ) >> 8 ) + 128;
			sum [ 1 ] += ( ( 112 * r [ 1 ] - 94 * g [ 1 ] - 18  * b [ 1 ] + 128 ) >> 8 ) + 128;

			( *U ) [ idx ] = sum [ 0 ] / 4;
			( *V ) [ idx ] = sum [ 1 ] / 4;

			idx++;
		}
		line += 2 * width * 3;
	}
}

void rgb2yuv420f ( u8* rgb, i32 width, i32 height, u8** Y, u8** U, u8** V )
{
	i32 idx = 0;
	f32 sum [ 2 ] = { 0 };
	i32   r [ 2 ] = { 0 };
	i32	  g [ 2 ] = { 0 };
	i32	  b [ 2 ] = { 0 };
	u8* pos = 0;
	u8* line = 0;

	*Y = ( u8* ) malloc ( width * height );
	*U = ( u8* ) malloc ( ( width / 2 ) * ( height / 2 ) );
	*V = ( u8* ) malloc ( ( width / 2 ) * ( height / 2 ) );

	idx = 0;
	line = rgb;

	// ITU-R
	for ( i32 h = 0; h < height; h++ )
	{
		pos = line;
		for ( i32 w = 0; w < width; w++ )
		{
			r [ 0 ] = *pos;
			pos++;

			g [ 0 ] = *pos;
			pos++;

			b [ 0 ] = *pos;
			pos++;
			( *Y ) [ idx ] = ( ( 0.299 * r [ 0 ] ) + ( 0.587 * g [ 0 ] ) + ( 0.114 * b [ 0 ] ) );
			idx++;
		}
		line += width * 3;
	}

	idx = 0; line = rgb;
	for ( i32 j = 0; j < height; j += 2 )
	{
		pos = line;
		for ( i32 i = 0; i < width; i += 2 )
		{
			r [ 0 ] = *pos;
			r [ 1 ] = *( pos + width * 3 );
			pos++;

			g [ 0 ] = *pos;
			g [ 1 ] = *( pos + width * 3 );
			pos++;

			b [ 0 ] = *pos;
			b [ 1 ] = *( pos + width * 3 );
			pos++;

			sum [ 0 ] =  -( 0.169 * r [ 0 ] ) - ( 0.331 * g [ 0 ] ) + ( 0.499  * b [ 0 ] ) + 128;
			sum [ 1 ] =   ( 0.499 * r [ 0 ] ) - ( 0.418 * g [ 0 ] ) - ( 0.0813 * b [ 0 ] ) + 128;

			sum [ 0 ] += -( 0.169 * r [ 1 ] ) - ( 0.331 * g [ 1 ] ) + ( 0.499  * b [ 1 ] ) + 128;
			sum [ 1 ] +=  ( 0.499 * r [ 1 ] ) - ( 0.418 * g [ 1 ] ) - ( 0.0813 * b [ 1 ] ) + 128;

			r [ 0 ] = *pos;
			r [ 1 ] = *( pos + width * 3 );
			pos++;

			g [ 0 ] = *pos;
			g [ 1 ] = *( pos + width * 3 );
			pos++;

			b [ 0 ] = *pos;
			b [ 1 ] = *( pos + width * 3 );
			pos++;

			sum [ 0 ] += -( 0.169 * r [ 0 ] ) - ( 0.331 * g [ 0 ] ) + ( 0.499  * b [ 0 ] ) + 128;
			sum [ 1 ] +=  ( 0.499 * r [ 0 ] ) - ( 0.418 * g [ 0 ] ) - ( 0.0813 * b [ 0 ] ) + 128;

			sum [ 0 ] += -( 0.169 * r [ 1 ] ) - ( 0.331 * g [ 1 ] ) + ( 0.499  * b [ 1 ] ) + 128;
			sum [ 1 ] +=  ( 0.499 * r [ 1 ] ) - ( 0.418 * g [ 1 ] ) - ( 0.0813 * b [ 1 ] ) + 128;

			( *U ) [ idx ] = ( sum [ 0 ] / 4.0f );
			( *V ) [ idx ] = ( sum [ 1 ] / 4.0f );

			idx++;
		}
		line += 2 * width * 3;
	}
}



void rgb2yuv ( u8* rgb, u32 width, u32 height, u32 subsampling, u8 **Y, u8 **U, u8 **V )
{
	if ( !rgb )
	{
		return;
	}
	
	u32 uv_vertical   = 0;
	u32 uv_horizontal = 0;
	
	switch ( subsampling )
	{
		case YUV444:
		{
			uv_vertical   = 4;
			uv_horizontal = 4;
		} break;
		
		case YUV440:
		{
			uv_vertical   = 2;
			uv_horizontal = 4;
		} break;
		
		case YUV422:
		{
			uv_vertical   = 4;
			uv_horizontal = 2;
		} break;
		
		case YUV420:
		{
			uv_vertical   = 2;
			uv_horizontal = 2;
		} break;
		
		case YUV411:
		{
			uv_vertical   = 4;
			uv_horizontal = 1;
		}break;
		
		case YUV410:
		{
			uv_vertical   = 2;
			uv_horizontal = 1;
		} break;
		
		default:
		{
			uv_vertical   = 2;
			uv_horizontal = 2;
		} break;
	}
	
    u32 uv_width  = width  * ( uv_horizontal / 4.0 );
    u32 uv_height = height * ( uv_vertical   / 4.0 );

	*Y = ( u8* ) malloc (    width * height    );
	*U = ( u8* ) malloc ( uv_width * uv_height );
	*V = ( u8* ) malloc ( uv_width * uv_height );

    for( u32 h = 0; h < height; h++ )
    {
        for( u32 w = 0; w < width; w++ )
        {
			u32  i  = ( w + h * width );
			u32  i2 = ( h * ( uv_vertical / 4.0 ) ) * ( width * ( uv_horizontal / 4.0 ) ) + ( w * ( uv_horizontal / 4.0 ) );
			
			u8 R = rgb [ i * 3 + 0 ];
			u8 G = rgb [ i * 3 + 1 ];
			u8 B = rgb [ i * 3 + 2 ];
			
			( *Y  ) [ i   ] =   ( 0.257 * R ) + ( 0.504 * G ) + ( 0.098 * B ) + 16;
			( *U  ) [ i2  ] =   ( 0.439 * R ) - ( 0.368 * G ) - ( 0.071 * B ) + 128; 
			( *V  ) [ i2  ] =  -( 0.148 * R ) - ( 0.291 * G ) + ( 0.439 * B ) + 128; 
        }
    }
    return;
}

double emboss_kernel [ 3 * 3 ] =
{
  -2.0, -1.0,  0.0,
  -1.0,  1.0,  1.0,
   0.0,  1.0,  2.0,
};

double sharpen_kernel [ 3 * 3 ] = 
{
  -1.0, -1.0, -1.0,
  -1.0,  9.0, -1.0,
  -1.0, -1.0, -1.0
};
double sobel_emboss_kernel [ 3 * 3 ] = 
{
  -1.0, -2.0, -1.0,
   0.0,  0.0,  0.0,
   1.0,  2.0,  1.0,
};

double box_blur_kernel [ 3 * 3 ] = 
{
  1.0, 1.0, 1.0,
  1.0, 1.0, 1.0,
  1.0, 1.0, 1.0,
};



f64* rgb2liear ( u8* rgb, u32 width, u32 height )
{
	f64* out = ( f64* ) malloc ( width * height * 3  );
	memset ( out, 0, width * height * 3 );

	for ( u32 h = 0; h < height; h++ )
	{
		for ( u32 w = 0; w < width; w++ )
		{
			u32 i = ( h * width + w ) * 3;
			out [ i + 0 ] = ( rgb [ i + 0 ] >= 11 ? pow ( rgb [ i + 0 ] * ( 40.0 / 10761.0 ) + ( 11.0 / 211.0 ), 2.4 ) : rgb [ i + 0 ] * ( 5.0 / 16473.0 ) );
			out [ i + 1 ] = ( rgb [ i + 1 ] >= 11 ? pow ( rgb [ i + 1 ] * ( 40.0 / 10761.0 ) + ( 11.0 / 211.0 ), 2.4 ) : rgb [ i + 1 ] * ( 5.0 / 16473.0 ) );
			out [ i + 2 ] = ( rgb [ i + 2 ] >= 11 ? pow ( rgb [ i + 2 ] * ( 40.0 / 10761.0 ) + ( 11.0 / 211.0 ), 2.4 ) : rgb [ i + 2 ] * ( 5.0 / 16473.0 ) );
		}
	}
	return out;
}



u8* sobel3x3 ( u8* data, u32 height, u32 width, u32 channels )
{
    // define kernals
    i32 Gy [ 9 ] = 
	{ 
		-1, 0, 1, 
		-2, 0, 2,
		-1, 0, 1 
	};

    i32 Gx [ 9 ] = 
	{ 
		-1, -2, -1, 
		 0,  0,  0,
		 1,  2,  1
	};

    i32 Gx_red, Gx_green, Gx_blue;
    i32 Gy_red, Gy_green, Gy_blue;
	
	u8* p   = data;
	u8* out = ( u8* ) malloc ( width * height * channels );
	assert ( out );
	memset ( out, 0, width * height * channels );

    for ( u32 h = 0; h < height; h++ )
    {
        for ( u32 w = 0; w < width; w++ )
        {
			u32 i  = ( h * width + w ) * channels;
            Gx_red = Gx_green = Gx_blue = 0;
            Gy_red = Gy_green = Gy_blue = 0;

            for ( i32 y = -1; y < 2; y++ )
            {
                for ( i32 x = -1; x < 2; x++ )
                {
					if ( ( h + y <= height ) && ( h + y >= 0 ) && ( w + x <= width ) && ( w + x >= 0 ) )
					{
						u32 index = ( ( h + y ) * width + ( x + w ) ) * channels;

						// Gx kernal
						Gx_red   += ( p [ index + 0 ] * Gx [ ( y + 1 ) * 3 + ( x + 1 ) ] );
						Gx_green += ( p [ index + 1 ] * Gx [ ( y + 1 ) * 3 + ( x + 1 ) ] );
						Gx_blue  += ( p [ index + 2 ] * Gx [ ( y + 1 ) * 3 + ( x + 1 ) ] );
						
						// Gy kernal
						Gy_red   += ( p [ index + 0 ] * Gy [ ( y + 1 ) * 3 + ( x + 1 ) ] );
						Gy_green += ( p [ index + 1 ] * Gy [ ( y + 1 ) * 3 + ( x + 1 ) ] );
						Gy_blue  += ( p [ index + 2 ] * Gy [ ( y + 1 ) * 3 + ( x + 1 ) ] );
					}
                }
            }

			out [ i + 0 ] = sqrt ( ( Gx_red   * Gx_red   ) + ( Gy_red   * Gy_red   ) );
            out [ i + 1 ] = sqrt ( ( Gx_green * Gx_green ) + ( Gy_green * Gy_green ) );
			out [ i + 2 ] = sqrt ( ( Gx_blue  * Gx_blue  ) + ( Gy_blue  * Gy_blue  ) );	
#if 0
			out [ i * channels + 0 ] = abs ( Gx_red   + Gy_red   ) / 2;
			out [ i * channels + 1 ] = abs ( Gx_green + Gy_green ) / 2;
			out [ i * channels + 2 ] = abs ( Gx_blue  + Gy_blue  ) / 2;
#endif
        }
    }
	return out;
}


TTF_Font* sdl_font_init ( const i8* font_name, u32 font_size )
{
	TTF_Init();
	TTF_Font* font = 0;
	font = TTF_OpenFont ( font_name, font_size );
	return font;
}

void render_text ( SDL_Renderer* renderer, TTF_Font *font, u32 font_color, u32 x, u32 y, const i8* fmt, ... )
{
	i8 text [ 2048 ] = { 0 };
	va_list ap;
	va_start ( ap, fmt );	
	vsnprintf ( text, sizeof ( text ), fmt, ap );
	va_end   ( ap );
	// SDL_Surface* surface = TTF_RenderUTF8_LCD ( font, text, ( SDL_Color ) { HEX_COLOR ( font_color ) }, ( SDL_Color ) { HEX_COLOR ( 0xFFFFFF00 ) } );
	SDL_Surface* surface = TTF_RenderUTF8_Blended ( font, text, ( SDL_Color ) { HEX_COLOR ( font_color ) } );
	SDL_Texture* texture = 0;
	texture = SDL_CreateTextureFromSurface ( renderer, surface );
	SDL_Rect rect = { x, y, surface->w, surface->h };	
	SDL_RenderCopy     ( renderer, texture, 0, &rect );
	SDL_DestroyTexture ( texture );
	SDL_FreeSurface    ( surface );
}


FORCEINLINE u64 get_time ( void )
{
	u64 counter   = 0;
	u64 frequency = 0;
	QueryPerformanceFrequency ( ( LARGE_INTEGER* ) &frequency );
	QueryPerformanceCounter   ( ( LARGE_INTEGER* ) &counter   );
	return ( counter * 1000 / frequency );
}


i32 main ( i32 argc, i8** argv )
{
	SDL_Window*   window   = 0;
	SDL_Renderer* renderer = 0;
	SDL_Texture*  yuv      = 0;
	SDL_Texture*  rgb      = 0;
	SDL_Texture*  diff     = 0;
	SDL_Texture*  img      = 0;
	SDL_Texture*  dva      = 0;

	
	SCC ( SDL_Init ( SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_EVENTS ) );
	SCP ( window = SDL_CreateWindow ( "YUV Player",
	                                  SDL_WINDOWPOS_CENTERED,  
									  SDL_WINDOWPOS_CENTERED, 
									  WINDOW_WIDTH,
									  WINDOW_HEIGHT,
									  SDL_WINDOW_RESIZABLE     |
									  SDL_WINDOW_ALLOW_HIGHDPI |
									  SDL_WINDOW_VULKAN ) );
											
	SCP ( renderer = SDL_CreateRenderer ( window, -1, SDL_RENDERER_ACCELERATED ) );	
	SCP ( rgb = SDL_CreateTexture       ( renderer, 
		                                  SDL_PIXELFORMAT_RGBA8888,
										  SDL_TEXTUREACCESS_STATIC | SDL_TEXTUREACCESS_TARGET,
										  CIF_WIDTH, 
										  CIF_HEIGHT ) );

	SCP ( yuv = SDL_CreateTexture  ( renderer,
									 SDL_PIXELFORMAT_IYUV,
									 SDL_TEXTUREACCESS_STATIC | SDL_TEXTUREACCESS_TARGET,
									 CIF_WIDTH,
									 CIF_HEIGHT ) );

	SCP ( diff = SDL_CreateTexture ( renderer,
									 SDL_PIXELFORMAT_IYUV,
									 SDL_TEXTUREACCESS_STATIC,
									 CIF_WIDTH,
									 CIF_HEIGHT ) );
									 
									 
									 
	TTF_Font* font = sdl_font_init ( "hack.ttf", 32 );
	assert ( font );
									 
	u32 imgw, imgh, c;
	u8 *data = stbi_load ( "vda.jpg", &imgw, &imgh, &c, 3 );
	
	u8 *IMGY = 0;
	u8 *IMGU = 0;
	u8 *IMGV = 0;
	
	u8* ptr = sobel3x3 ( data, imgw, imgh, 3 );
	// u8* linear = rgb2liear ( data, imgw, imgh );
	
	// rgba2yuv420 ( data, imgw, imgh, &IMGY, &IMGU, &IMGV );
	rgb2yuv420f ( data, imgw, imgh, &IMGY,  &IMGU, &IMGV );
	rgb2yuv     ( ptr,  imgw, imgh, YUV444, &IMGY, &IMGU, &IMGV );
	// stbi_image_free ( data );							
	
									 
	SCP ( img = SDL_CreateTexture  ( renderer,
									 SDL_PIXELFORMAT_IYUV,
									 SDL_TEXTUREACCESS_STATIC,
									 imgw,
									 imgh ) );
	
	SCP ( dva = SDL_CreateTexture ( renderer,
		                            SDL_PIXELFORMAT_RGB24,
									SDL_TEXTUREACCESS_STATIC,
									imgw,
									imgh ) );

	u64 size = 0;
	u8* buffer = ( u8* ) mmap ( "recon.yuv", &size );
	u8* p = buffer;
	b32 done = false;
	
	i32 cpu [ 4 ] = { 0 };
	__cpuid ( cpu, 0 );
	

	while ( !done )
	{
		u64 start = get_time ( );
		SDL_Event e = { 0 };
		while ( SDL_PollEvent ( &e ) )
		{
			if ( e.type == SDL_QUIT )
			{
				done = true;
			}			
			
			if ( e.type == SDL_KEYDOWN )
			{
				if ( e.key.keysym.sym == SDLK_ESCAPE )
				{
					done = true;
				}
			}
		}
		

		u64 cif_y_size = ( CIF_WIDTH * CIF_HEIGHT );
		u64 cif_uv_size = ( ( CIF_WIDTH / 2 ) * ( CIF_HEIGHT / 2 ) );

		if ( p == ( buffer + size ) )
		{
			p = buffer;
		}


		memcpy ( Y1, p, cif_y_size );
		p += cif_y_size;
		memcpy ( U1, p, cif_uv_size );
		p += cif_uv_size;
		memcpy ( V1, p, cif_uv_size );
		p += cif_uv_size;

		if ( p != ( buffer + size ) )
		{
			memcpy ( Y2, p, cif_y_size );
			memcpy ( U2, p + cif_y_size, cif_uv_size );
			memcpy ( V2, p + cif_y_size + cif_uv_size, cif_uv_size );
		}
		else
		{
			memcpy ( Y2, Y1, cif_y_size );
			memcpy ( U2, U1, cif_uv_size );
			memcpy ( V2, V1, cif_uv_size );
		}

		// luma_dct ( Y1, CIF_WIDTH, CIF_HEIGHT, 8 );


		// Let's Start Diffing
		for ( u32 h = 0; h < CIF_HEIGHT; h++ )
		{
			for ( u32 w = 0; w < CIF_WIDTH; w++ )
			{
				u32 i  = h * CIF_WIDTH + w;
				u32 hi = ( h / 2 ) * ( CIF_WIDTH / 2 ) + ( w / 2 );
				Y2 [  i ] = abs ( Y1 [ i  ] - Y2 [ i  ] );
				U2 [ hi ] = abs ( U1 [ hi ] - U2 [ hi ] );
				V2 [ hi ] = abs ( V1 [ hi ] - V2 [ hi ] );
			}
		}

		SDL_Rect rgb_rect  = { 0,    0,   352,  288  };
		SDL_Rect yuv_rect  = { 352,  0,   352,  288  };
		SDL_Rect dva_rect  = { 704,  0,   imgw, imgh };
		SDL_Rect diff_rect = { 0,    288, 352,  288  };
		SDL_Rect img_rect  = { 352,  288, imgw, imgh };
		SDL_Rect rect      = { 0,    0,   352,  288  };




		SCC ( SDL_UpdateYUVTexture ( yuv,
									 0,
									 Y1, CIF_WIDTH,
									 U1, CIF_WIDTH / 2,
									 V1, CIF_WIDTH / 2  ) );

		SCC ( SDL_SetRenderTarget ( renderer, yuv ) );
		SDL_SetRenderDrawColor ( renderer, HEX_COLOR ( 0xFF2222FF ) );

		for ( u32 y = 0; y < CIF_HEIGHT; y += 8 )
		{
			SDL_RenderDrawLine ( renderer, 0, y, CIF_WIDTH, y );
		}

		for ( u32 x = 0; x < CIF_WIDTH + 1; x += 8 )
		{
			SDL_RenderDrawLine ( renderer, x, 0, x, CIF_HEIGHT );
		}

		SCC ( SDL_SetRenderTarget ( renderer, 0 ) );

									 
		SCC ( SDL_UpdateYUVTexture ( img,
									 0,
									 IMGY, imgw,
									 IMGU, imgw / 2,
									 IMGV, imgw / 2  ) );

		SCC ( SDL_UpdateYUVTexture ( diff,
									 0,
									 Y2, CIF_WIDTH,
									 U2, CIF_WIDTH / 2,
									 V2, CIF_WIDTH / 2 ) );


		for ( u32 h = 0; h < CIF_HEIGHT; h++ )
		{
			for ( u32 w = 0; w < CIF_WIDTH; w++ )
			{
				u32 hi = ( h / 2 ) * ( CIF_WIDTH / 2 ) + ( w / 2 );
				u32 i = w + ( h * CIF_WIDTH );

				u8 r = Y1 [ i ] + ( 1.370705 * ( V1 [ hi ] - 128 ) );
				u8 g = Y1 [ i ] - ( 0.698001 * ( V1 [ hi ] - 128 ) ) - ( 0.337633 * ( U1 [ hi ] - 128 ) );
				u8 b = Y1 [ i ] + ( 1.732446 * ( U1 [ hi ] - 128 ) );
				u8 a = 0xFF;

				RGB [ i * 4 + 0 ] = a;
				RGB [ i * 4 + 1 ] = r;
				RGB [ i * 4 + 2 ] = r;
				RGB [ i * 4 + 3 ] = r;
			}
		}

		u32 pitch = CIF_WIDTH * 4;
		SCC ( SDL_UpdateTexture ( rgb, 0, RGB, pitch ) );

		SCC ( SDL_SetRenderTarget ( renderer, rgb ) );
		SDL_SetRenderDrawColor ( renderer, HEX_COLOR ( 0xFB4934FF ) );
		for ( u32 y = 0; y < CIF_HEIGHT; y += 16 )
		{
			SDL_RenderDrawLine ( renderer, 0, y, CIF_WIDTH, y );
		}
		for ( u32 x = 0; x < CIF_WIDTH; x += 16 )
		{
			SDL_RenderDrawLine ( renderer, x, 0, x, CIF_HEIGHT );
		}
		SCC ( SDL_SetRenderTarget ( renderer, 0 ) );

		SCC ( SDL_UpdateTexture ( dva, 0, data, ( imgw * 3 ) ) );




		screen_clear      ( renderer, 0x282828FF );
		SDL_RenderCopy    ( renderer, rgb, &rect,  &rgb_rect );
		SDL_RenderCopy    ( renderer, yuv, &rect,  &yuv_rect );
		SDL_RenderCopy    ( renderer, diff, &rect, &diff_rect );
		SDL_RenderCopy    ( renderer, img, 0,      &img_rect );
		SDL_RenderCopy    ( renderer, dva, 0,      &dva_rect );
		
		u64 end = get_time ( );

		render_text ( renderer, font, 0x00FF00FF, 10, 10, "FPS: %d", ( end - start ) );

		SDL_RenderPresent ( renderer             );
		// SDL_Delay ( 100 );
	}
	
	munmap ( buffer );
	SDL_DestroyRenderer ( renderer );
    SDL_DestroyWindow   ( window   );
	SDL_Quit ( );
	return 0;
}

