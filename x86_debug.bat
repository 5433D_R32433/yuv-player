@echo off

if not defined DevEnvDir (
call vc x86
)

set CFLAGS= -MTd -Z7 -GS- -Oi -Gm- -GR- -sdl- -FC -I..
set LFLAGS= -link -heap:134217728,67108864 -stack:33554432,16777216 -SUBSYSTEM:CONSOLE -NXCOMPAT:NO -DYNAMICBASE:NO -opt:ref -opt:icf -out:output.exe
set WIN32_LIBS= opengl32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib ws2_32.lib crypt32.lib setupapi.lib imm32.lib winmm.lib version.lib d2d1.lib ole32.lib Netapi32.lib userenv.lib dwmapi.lib wtsapi32.lib
set LIBS=
set SOURCES= ../../main.c

if exist debug\x86 (
rmdir /S /Q debug\x86
mkdir debug\x86
pushd debug\x86
cl %CFLAGS% %SOURCES% %LFLAGS% %WIN32_LIBS% %LIBS%
popd debug\x86
) else (
mkdir debug\x86
pushd debug\x86
cl %CFLAGS% %SOURCES% %LFLAGS% %WIN32_LIBS% %LIBS%
popd debug\x86
)




