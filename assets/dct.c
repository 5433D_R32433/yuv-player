#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#define M_PI 3.14159265359f

void dct           ( float **DCTMatrix, float **Matrix,    int N, int M );
void write_mat     ( FILE *fp,          float **testRes,   int N, int M );
void idct          ( float **Matrix,    float **DCTMatrix, int N, int M );
float **calloc_mat ( int dimX, int dimY );
void free_mat      ( float **p          );


float **calloc_mat ( int dimX, int dimY )
{
    float **m = calloc ( dimX,        sizeof ( float* ) );
    float *p  = calloc ( dimX * dimY, sizeof ( float  ) );
    for ( int i = 0; i < dimX; i++ )
	{
		m [ i ] = &p [ i * dimY ];
    }
	return m;
}

void free_mat ( float **m )
{
  free ( m [ 0 ] );
  free ( m );
}

void write_mat ( FILE *fp, float **m, int N, int M )
{
   for ( int i = 0; i < N; i++ )
   {
		fprintf ( fp, "%f", m [ i ] [ 0 ] );
		for ( int j = 1; j < M; j++ )
		{
		   fprintf ( fp, "\t%f", m [ i ] [ j ] );
		}   
		fprintf ( fp, "\n" );
   }
   fprintf ( fp, "\n" );
}

void dct ( float **DCTMatrix, float **Matrix, int N, int M )
{
    for ( int u = 0; u < N; ++u ) 
	{
        for ( int v = 0; v < M; ++v ) 
		{
			DCTMatrix [ u ] [ v ] = 0;
            for ( int i = 0; i < N; i++ ) 
			{
                for ( int j = 0; j < M; j++ ) 
				{
                    DCTMatrix [ u ] [ v ] += 
					Matrix [ i ] [ j ] * cos ( M_PI / ( ( float ) N ) * ( i + 1.0f / 2.0f ) * u ) 
					                   * cos ( M_PI / ( ( float ) M ) * ( j + 1.0f / 2.0f ) * v );
                }               
            }
        }
    }  
}

void idct ( float **Matrix, float **DCTMatrix, int N, int M )
{
    for ( int u = 0; u < N; ++u ) 
	{
			for ( int v = 0; v < M; ++v ) 
			{
				Matrix [ u ] [ v ] = 1.0f / 4.0f * DCTMatrix [ 0 ] [ 0 ];		
				for ( int i = 1; i < N; i++ )
				{
					Matrix [ u ] [ v ] += 1.0f / 2.0f * DCTMatrix [ i ] [ 0 ];
				}			  
				for ( int j = 1; j < M; j++ )
				{
					Matrix [ u ] [ v ] += 1.0f / 2.0f * DCTMatrix [ 0 ] [ j ];
				}			  
				for ( int i = 1; i < N; i++ ) 
				{
					for ( int j = 1; j < M; j++ ) 
					{
						Matrix [ u ] [ v ] += DCTMatrix [ i ] [ j ] * 
						                      cos ( M_PI / ( ( float ) N ) * ( u + 1.0f / 2.0f ) * i ) * 
											  cos ( M_PI / ( ( float ) M ) * ( v + 1.0f / 2.0f ) * j );
					}               
				}			
				Matrix [ u ] [ v ] *= 2.0f / ( ( float ) N ) * 2.0f / ( ( float ) M );
			}	
    }  
 }



int main ( ) 
{

   float 
   testBlockA [ 8 ] [ 8 ] = 
	{ 
		{ 255, 255, 255, 255, 255, 255, 255, 255 },
		{ 255, 255, 255, 255, 255, 255, 255, 255 },
		{ 255, 255, 255, 255, 255, 255, 255, 255 },
		{ 255, 255, 255, 255, 255, 255, 255, 255 },
		{ 255, 255, 255, 255, 255, 255, 255, 255 },
		{ 255, 255, 255, 255, 255, 255, 255, 255 },
		{ 255, 255, 255, 255, 255, 255, 255, 255 },
		{ 255, 255, 255, 255, 255, 255, 255, 255 } 
	},

    testBlockB [ 8 ] [ 8 ] = 
	{
		{ 255, 0,   255, 0,   255, 0,   255, 0   },
		{ 0,   255, 0,   255, 0,   255, 0,   255 },
		{ 255, 0,   255, 0,   255, 0,   255, 0   },
		{ 0,   255, 0,   255, 0,   255, 0,   255 },
		{ 255, 0,   255, 0,   255, 0,   255, 0   },
		{ 0,   255, 0,   255, 0,   255, 0,   255 },
		{ 255, 0,   255, 0,   255, 0,   255, 0   },
		{ 0,   255, 0,   255, 0,   255, 0,   255 } 
	};

    FILE * fp = fopen ( "mydata.csv", "w" );
    int dimX = 8;
	int	dimY = 8;

    float **testBlock = calloc_mat ( dimX, dimY );
    float **testDCT   = calloc_mat ( dimX, dimY );
    float **testiDCT  = calloc_mat ( dimX, dimY );

    for ( int i = 0; i < dimX; i++ )
	{
		for ( int j = 0; j < dimY; j++ )
		{
			testBlock [ i ] [ j ] = testBlockB [ i ] [ j ];
		}
    }
	
	write_mat ( fp, testBlock, dimX, dimY );

    dct       ( testDCT, testBlock, dimX, dimY );
    write_mat ( fp, testDCT, dimX, dimY        );
	
    idct      ( testiDCT, testDCT, dimX, dimY );
    write_mat ( fp, testiDCT, dimX, dimY      );

    fclose    ( fp        );
    free_mat  ( testBlock );
    free_mat  ( testDCT   );
    free_mat  ( testiDCT  );

    return 0;
}