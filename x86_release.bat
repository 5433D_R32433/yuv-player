@echo off

if not defined DevEnvDir (
call vc x86
)

set CFLAGS= -MT -GS- -O2 -Oi -Gm- -GR- -sdl- -FC -I..


set LFLAGS= -link -SUBSYSTEM:console -EMITPOGOPHASEINFO -DEBUG:NONE -NXCOMPAT:NO -DYNAMICBASE:NO -opt:ref -opt:icf -out:output.exe 
set LIBS= 
set SOURCES= ../../main.c

if exist release\x86 (
rmdir /S /Q release\x86
mkdir release\x86
pushd release\x86
cl %CFLAGS% %SOURCES% %LFLAGS% %WIN32_LIBS% %LIBS%
popd release\x86
) else (
mkdir release\x86
pushd release\x86
cl %CFLAGS% %SOURCES% %LFLAGS% %WIN32_LIBS% %LIBS%
popd release\x86
)




