@echo off

if not defined DevEnvDir (
call vc x64
)

set CFLAGS= -MT -GS- -O2 -Oi -Gm- -GR- -sdl- -FC -I..

set LFLAGS= -link -SUBSYSTEM:console -EMITPOGOPHASEINFO -DEBUG:NONE -NXCOMPAT:NO -DYNAMICBASE:NO -opt:ref -opt:icf -out:output.exe 
set WIN32_LIBS= opengl32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib ws2_32.lib crypt32.lib setupapi.lib imm32.lib winmm.lib version.lib d2d1.lib ole32.lib Netapi32.lib userenv.lib dwmapi.lib wtsapi32.lib
set LIBS=
set SOURCES= ../../main.c

if exist release\x64 (
rmdir /S /Q release\x64
mkdir release\x64
pushd release\x64
cl %CFLAGS% %SOURCES% %LFLAGS% %WIN32_LIBS% %LIBS%
popd release\x64
) else (
mkdir release\x64
pushd release\x64
cl %CFLAGS% %SOURCES% %LFLAGS% %WIN32_LIBS% %LIBS%
popd release\x64
)




